# Kaggle Career con - Help Navigate a Robot

The task is to predict which one of the nine-floor types (carpet, tiles, concrete) the robot is on using sensor data such as acceleration and velocity. The data is based on IMU Sensor Data. This is gotten from an ongoing Kaggle competition